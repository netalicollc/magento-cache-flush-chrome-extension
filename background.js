chrome.commands.onCommand.addListener(function(command) {
  var hostname = '';
  var magentoVersion = '';

  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
     var activeTab = tabs[0];
     var activeTabId = activeTab.id;
    hostname = getHostname(activeTab.url);


    var magentoVersion = getMagentoVersion(activeTab)


    chrome.windows.getAll({populate:true},function(windows){
      windows.forEach(function(window){
        window.tabs.forEach(function(tab){

          const code = 'document.querySelectorAll("head")[0].innerHTML';
          chrome.tabs.executeScript(tab.id, { code } , function(text) {
            var text = "" + text;
            if (text.indexOf("pub/static") !== -1 ) {
              //m2
              if ((tab.url.indexOf(hostname)  >= 0) && (tab.url.indexOf("cache")  >= 0)) {
                chrome.tabs.executeScript(tab.id, {file: 'm2.js'})
              }
            } else {
              // m1
              if ((tab.url.indexOf(hostname + '/index.php')  >= 0) && (tab.url.indexOf("cache")  >= 0)) {
                chrome.tabs.executeScript(tab.id, {file: 'm1.js'})
              }
            }

          });


      });
    });
});
  });

});


function getMagentoVersion(tab) {

  // return version;
}

function getHostname(url) {
	// Handle Chrome URLs
	if (/^chrome:\/\//.test(url)) { return; }
	try {
		var newUrl = new URL(url);
		return newUrl.hostname;
	} catch (err) {
		console.log(err);
	}
}
